FROM node:12.21-buster
WORKDIR /node_app
COPY package*.json /node_app/
RUN npm install
RUN npm install pm2 -g
COPY . /node_app/
EXPOSE 3000
CMD  [ "pm2-runtime","start","deploy.json" ]

# Creation d'une image Docker: docker build -t image_name:version (exp:1.0) /path/to/dockerfile
# Creation d'un Container: docker run -it --name container_name:container_version -p 4000:3000 (host:docker) image_name:version
# Creation d'un Container: docker run -it --name container_name:container_version -v /path/local:/path/container -p 4000:3000 (host:docker) image_name:version
#  from les langages/framework qu'ona 

